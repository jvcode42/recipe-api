terraform {
  backend "s3" {
    bucket         = "aws-jv-s3-terraform-state"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "aws_jv_dynamo_terraform_statelock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.global-prefix}-${terraform.workspace}-${var.app-prefix}"
  common_tags = {
    Environment = terraform.workspace
    Application = var.application
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}