#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.services
sudo systemctl start docker.services
sudo usermod -aG docker ec2-usermod