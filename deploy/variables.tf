variable "global-prefix" {
  default = "awsjv"
}

variable "app-prefix" {
  default = "rp"
}

variable "application" {
  default = "recipe-app"
}

variable "contact" {
  default = "aws@jochen-vajda.de"
}

variable "db_username" {
  description = "Username for the RDS PostgreSQL instance"
}

variable "db_password" {
  description = "Password for the RDS PostgreSQL instance"
}

variable "bastion_ssh-key_name" {
  default = "ssh-key-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "440614756468.dkr.ecr.us-east-1.amazonaws.com/aws-recipe-app:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "440614756468.dkr.ecr.us-east-1.amazonaws.com/aws-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain Name"
  default     = "darth-vajda.de"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api",
    staging    = "api.staging",
    dev        = "api.dev"
  }
}